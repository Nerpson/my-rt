#pragma once
#include "pch.h"
#include "../MyRT/Vec3.h"
#include "../MyRT/Quaternion.h"

TEST(Quaternion, FromAngles)
{
	Quaternion q = Quaternion::from_angles(Angle::from_degrees(1.), Angle::from_degrees(2.), Angle::from_degrees(3.));
	ASSERT_NEAR(q.w, 0.9994630275083051, 0.001);
	ASSERT_NEAR(q.x, 0.026324211700910347, 0.001);
	ASSERT_NEAR(q.y, -0.008265383148751184, 0.001);
	ASSERT_NEAR(q.z, 0.017674160904072974, 0.001);
}
