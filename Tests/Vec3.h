#pragma once
#include "pch.h"
#include <numbers>
#include "../MyRT/Vec3.h"

TEST(Vec3, FromString)
{
	ASSERT_EQ(Vec3::from_string("1 2.3  -4"), Vec3(1., 2.3, -4.));
}

TEST(Vec3, Distance)
{
	Vec3 a(1., 2., 3.);
	Vec3 b(4., 5., 6.);
	ASSERT_EQ(Vec3::distance(a, b), 5.19615221f);
}

TEST(Vec3, Dot)
{
	Vec3 a(1., 2., 3.);
	Vec3 b(4., 5., 6.);
	ASSERT_EQ(Vec3::dot(a, b), 32.);
}

TEST(Vec3, Cross)
{
	Vec3 a(1., 2., 3.);
	Vec3 b(4., 5., 6.);
	ASSERT_EQ(Vec3::cross(a, b), Vec3(-3., 6., -3.));
}

TEST(Vec3, Getters)
{
	constexpr double x = 1.;
	constexpr double y = 2.;
	constexpr double z = 3.;
	Vec3 a(x, y, z);
	ASSERT_EQ(a.x(), x);
	ASSERT_EQ(a.y(), y);
	ASSERT_EQ(a.z(), z);
}

TEST(Vec3, Norm)
{
	Vec3 a(2., 2., 1.);
	ASSERT_EQ(a.norm(), 3.);
}

TEST(Vec3, IsUnit)
{
	Vec3 a(1., 1., 1.);
	ASSERT_FALSE(a.is_unit());

	Vec3 b(0., 1., 0.);
	ASSERT_TRUE(b.is_unit());
}

TEST(Vec3, ToUnit)
{
	Vec3 a(42., 0., 0.);
	ASSERT_EQ(a.to_unit(), Vec3(1., 0., 0.));
}

TEST(Vec3, Rotated0)
{
	Vec3 a(1., 0., 0.);
	Vec3 rotated_a = a.rotated(Angle(), Angle(), Angle());

	ASSERT_FLOAT_EQ(a.x(), rotated_a.x());
	ASSERT_FLOAT_EQ(a.y(), rotated_a.y());
	ASSERT_FLOAT_EQ(a.z(), rotated_a.z());
}

TEST(Vec3, Rotated360)
{
	Vec3 a(1., 0., 0.);
	Vec3 rotated_a = a.rotated(Angle::from_radians(2. * std::numbers::pi_v<double>), Angle(), Angle());

	ASSERT_NEAR(a.x(), rotated_a.x(), 0.0001);
	ASSERT_NEAR(a.y(), rotated_a.y(), 0.0001);
	ASSERT_NEAR(a.z(), rotated_a.z(), 0.0001);
}

TEST(Vec3, Rotated90)
{
	Vec3 a = Vec3::up();
	Vec3 rotated_a = a.rotated(Angle(), Angle::from_degrees(90.), Angle());
	Vec3 expected_a_result(1., 0., 0.);

	ASSERT_NEAR(expected_a_result.x(), rotated_a.x(), 0.0001);
	ASSERT_NEAR(expected_a_result.y(), rotated_a.y(), 0.0001);
	ASSERT_NEAR(expected_a_result.z(), rotated_a.z(), 0.0001);

	Vec3 b = Vec3::forward();
	Vec3 rotated_b = b.rotated(Angle(), Angle::from_degrees(90.), Angle());
	Vec3 expected_b_result(0., 0., -1.);

	ASSERT_NEAR(expected_b_result.x(), rotated_b.x(), 0.0001);
	ASSERT_NEAR(expected_b_result.y(), rotated_b.y(), 0.0001);
	ASSERT_NEAR(expected_b_result.z(), rotated_b.z(), 0.0001);

	Vec3 c = Vec3::right();
	Vec3 rotated_c = c.rotated(Angle(), Angle::from_degrees(90.), Angle());
	Vec3 expected_c_result = c;

	ASSERT_NEAR(expected_c_result.x(), rotated_c.x(), 0.0001);
	ASSERT_NEAR(expected_c_result.y(), rotated_c.y(), 0.0001);
	ASSERT_NEAR(expected_c_result.z(), rotated_c.z(), 0.0001);
}

TEST(Vec3, Add)
{
	Vec3 a(0., -1., 2.);
	Vec3 b(1., 43., -5.);
	ASSERT_EQ(a + b, Vec3(1., 42., -3.));
}

TEST(Vec3, Substract)
{
	Vec3 a(0., 2., 4.);
	Vec3 b(8., 16., 32.);
	ASSERT_EQ(a - b, Vec3(-8., -14., -28.));
}

TEST(Vec3, Multiply)
{
	Vec3 a(0., 1., -2.);
	double b = -2.5;
	ASSERT_EQ(a * b, Vec3(0., -2.5, 5.));
}

TEST(Vec3, Divide)
{
	Vec3 a(0., 1., -2.);
	double b = 2.;
	ASSERT_EQ(a / b, Vec3(0., 0.5, -1.));
}

TEST(Vec3, Equals)
{
	Vec3 a(0., 1., -2.);
	ASSERT_TRUE(a == a);
}