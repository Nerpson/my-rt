#pragma once
#include "pch.h"
#include "../MyRT/Angle.h"
#include <numbers>

TEST(Angle, FromRadians)
{
	Angle a = Angle::from_radians(std::numbers::pi_v<double> / 2.);
	ASSERT_DOUBLE_EQ(a.degrees(), 90.);

	Angle b = Angle::from_radians(std::numbers::pi_v<double> / 4.);
	ASSERT_DOUBLE_EQ(b.degrees(), 45.);
}

TEST(Angle, FromDegrees)
{
	Angle a = Angle::from_degrees(90.);
	ASSERT_DOUBLE_EQ(a.radians(), std::numbers::pi_v<double> / 2.);
	
	Angle b = Angle::from_degrees(45.);
	ASSERT_DOUBLE_EQ(b.radians(), std::numbers::pi_v<double> / 4.);
}