#include "pch.h"
#include "Quaternion.h"
#include "Angle.h"
#include "Vec3.h"
#include "../MyRT/Ray.h"
#include "../MyRT/Plane.h"
#include "../MyRT/Sphere.h"

TEST(Color, Getters)
{
	double r = 0.;
	double g = 0.5;
	double b = 1.;
	Color a(r, g, b);
	ASSERT_EQ(a.r(), r);
	ASSERT_EQ(a.g(), g);
	ASSERT_EQ(a.b(), b);
}

TEST(Color, ToRanged)
{
	Color a(0., 1., 2.);
	a = a.to_ranged(0., 1.);
	ASSERT_EQ(a.r(), 0.);
	ASSERT_EQ(a.g(), 1.);
	ASSERT_EQ(a.b(), 1.);
}

TEST(Color, FromWeb)
{
	Color a = Color::from_web("#08F");
	ASSERT_FLOAT_EQ(a.r(), 0.);
	ASSERT_FLOAT_EQ(a.g(), (136 / 255.));
	ASSERT_FLOAT_EQ(a.b(), 1.);

	Color b = Color::from_web("#408015");
	ASSERT_FLOAT_EQ(b.r(), (64. / 255));
	ASSERT_FLOAT_EQ(b.g(), (128. / 255.));
	ASSERT_FLOAT_EQ(b.b(), (21. / 255.));
}

TEST(Color, Add)
{
	Color a(0., 1., 0.5);
	Color b(1., 1., 1.);
	ASSERT_EQ(a + b, Color(1., 2., 1.5));
}

TEST(Color, MultiplyByColor)
{
	Color a(0., 1., 0.5);
	Color b(1., 1., 1.);
	ASSERT_EQ(a * b, Color(0., 1., 0.5));
}

TEST(Color, MultiplyByFloat)
{
	Color a(0., 1., 0.5);
	ASSERT_EQ(a * 0.5, Color(0., 0.5, 0.25));
}

TEST(Color, Equals)
{
	Color a(0., 1., 0.5);
	ASSERT_TRUE(a == a);

	Color b(0., 1., 0.5);
	ASSERT_TRUE(a == b);
}

TEST(Plane, Intersection)
{
	Scene::Plane plane;
	plane.set_position(Vec3(0., 0., 0.));

	Vec3 origin(42., 420., 69.);
	Vec3 direction(0., -1., 0.);
	Ray::Ray ray(origin, direction);
	Vec3 intersection;

	plane.intersects(ray, intersection);
	ASSERT_EQ(intersection, Vec3(42., 0., 69.));
}

TEST(Sphere, IntersectionWith1Solution)
{
	Scene::Sphere sphere;
	sphere.set_position(Vec3(0., 0., 0.));
	sphere.set_radius(1.);

	Vec3 ray_origin(1., 2., 0.);
	Vec3 ray_direction(0., -1., 0.);
	Ray::Ray ray(ray_origin, ray_direction);
	Vec3 intersection;

	ASSERT_TRUE(sphere.intersects(ray, intersection));
	ASSERT_EQ(intersection, Vec3(1., 0., 0.));
}

TEST(Sphere, IntersectionWith2Solutions)
{
	Scene::Sphere sphere;
	sphere.set_position(Vec3(1., 0., 2.));
	sphere.set_radius(1.);

	Vec3 ray_origin(1., 2., 2.);
	Vec3 ray_direction(0., -1., 0.);
	Ray::Ray ray(ray_origin, ray_direction);
	Vec3 intersection;

	ASSERT_TRUE(sphere.intersects(ray, intersection));
	ASSERT_EQ(intersection, Vec3(1., 1., 2.));
}