# My Ray Tracer

## Features

- Objects
  - Plane
  - Sphere
- Camera
- Materials
  - Diffuse color
  - Specular
- Stochastic RT
- Multithreading
- XML Configuration

## Get started

Make sure you have a compiler supporting C++20 (`c++latest`).

Open the solution file in Visual Studio (`MyRT.sln`) and compile the whole solution.

If possible, run the unit tests in the project Tests, through Visual Studio's Test Explorer panel.

Create a `config.xml` file, it will store the application's configuration. Two configuration examples are provided: `marbles.xml` and `mercure_drops.xml`, you can copy the contents if you want to start with these.

Start the application and wait for the render to complete.

## XML Configuration

### Schema

The XML configuration file comes with a schema to validate it. Make sure to use it to have error highlighting and documentation in smart text editors, such as VS Code.

The file is available as `configuration.schema.xsd`.

### Structure

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="../configuration.schema.xsd"
	threads="4" bounces="6">
	<output width="1920" height="1080" file="output.bmp"/>
	<scene backdrop="#001">
		<materials>
			<material id="solid_white" diffuse="#fff"/>
			<material id="reflective_blue" diffuse="#108aff" specular="0.5"/>
		</materials>
		<camera pos="0 1 0" yaw="0" pitch="0" roll="0"/>
		<lights>
			<light id="main" pos="-2 1 0" intensity="20" color="#fff" radius="2" samples="4"/>
		</lights>
		<objects>
            <plane pos="0 0 0" material="solid_white"/>
            <sphere pos="0 0.5 0" radius="0.5" material="reflective_blue"/>
		</objects>
	</scene>
</configuration>
```

The XML root has to be a `configuration` tag, you can specify the XSD. Two attributes are mandatory: `threads`, and `bounces`. The first defines the number of threads to use to render the scene. You can look up in the Task Manager, Performance tab, the number of cores you have, and write the same number to use all of your CPU's cores. `bounces` defines the maximum depth of the light bounces. Writing 6 will allow 6 light bounces before returning the backdrop color.

The first child of the `configuration` tag is `output`, it comes with three attributes. `width` and `height` define the output image size, and `file` defines the path to the output file.

The third and last `configuration` child is `scene`, with a `backdrop` attribute defining the color of the background. The color has to be written as a hexadecimal color (`#xxx` or `#xxxxxx`), like it can be written in CSS.

The `scene` tag has four mandatory children. `materials`, `camera`, `lights`, and `objects`.

The `materials` tag contains `material` tags. These define the various materials to be used by the objects in the scene. A `material` tag has two mandatory attributes: `id` which defines the name of the material and `diffuse` which defines the color of the object, as a hexadecimal color. A `specular` attribute can also be written with a real value between `0` and `1`, it defines the amount of reflected light.

The `camera` tag defines the properties of the camera used to render the scene. The `pos` attribute contains three space-separated real values for each coordinate X Y and Z. The `yaw`, `pitch` and `roll` attributes define the rotation of the camera with Euler angles.

The `lights` tag contains `light` children, defining the lights in the scene. Each `light` tag has mandatory `pos`, `intensity`, `radius` and `samples` attributes. The `radius` attribute defines the virtual radius of the light, used for stochastic ray tracing, when the `samples` attribute is greater than `1`. An optional `id` attribute can be defined to debug or just to identify what the light is for.

The `objects` tag can contain two types of child. Each child comes with a mandatory `pos` attribute as well as an `id` text attribute, to identify the object, a `hidden` boolean attribute to change the visibility the object, and the usual `yaw` `pitch` `roll` attributes to define the Euler angles.

- `plane` defines an infinite plane
- `sphere` defines a sphere with a radius defined in the `radius` attribute (default value: `1`).

## Coordinates system

<img src="noun_directions_1527493.png" alt="Coordinates" width="128" height="128"/>

- X defines left/right movements,
- Y defines the height,
- Z defines backwards/forward movements.