#include "Vec3.h"
#include <cmath>
#include <regex>
#include "Quaternion.h"
#include "Random.h"

Vec3 Vec3::from_string(const std::string& _pos_string)
{
	std::regex regex("^(-?(?:\\d*\\.\\d+|\\d+\\.?))\\s+(-?(?:\\d*\\.\\d+|\\d+\\.?))\\s+(-?(?:\\d*\\.\\d+|\\d+\\.?))$", std::regex_constants::icase);
	std::smatch regex_match_result;
	if (!std::regex_match(_pos_string, regex_match_result, regex)) throw std::runtime_error("Position is not valid");
	return Vec3(
		std::stod(regex_match_result[1]),
		std::stod(regex_match_result[2]),
		std::stod(regex_match_result[3])
	);
}

Vec3 Vec3::random_filled(double _lower_bound, double _upper_bound)
{
	return Vec3(
		Random::get(_lower_bound, _upper_bound),
		Random::get(_lower_bound, _upper_bound),
		Random::get(_lower_bound, _upper_bound)
	);
}

double Vec3::distance(const Vec3& _a, const Vec3& _b)
{
	return (_a - _b).norm();
}

double Vec3::dot(const Vec3& _a, const Vec3& _b)
{
	return _a.x() * _b.x() + _a.y() * _b.y() + _a.z() * _b.z();
}

Vec3 Vec3::cross(const Vec3& _a, const Vec3& _b)
{
	return Vec3(
		_a.m_y * _b.m_z - _b.m_y * _a.m_z,
		_a.m_z * _b.m_x - _b.m_z * _a.m_x,
		_a.m_x * _b.m_y - _b.m_x * _a.m_y
	);
}

double Vec3::norm() const
{
	return sqrt(pow(m_x, 2.) + pow(m_y, 2.) + pow(m_z, 2.));
}

bool Vec3::is_unit() const
{
	return norm() == 1.;
}

Vec3 Vec3::to_unit() const
{
	return *this / this->norm();
}

Vec3 Vec3::rotated(const Angle& _yaw, const Angle& _pitch, const Angle& _roll) const
{
	return Quaternion::from_angles(_yaw, _pitch, _roll).rotate(*this);
}
	
void Vec3::operator+=(const Vec3& _other)
{
	m_x += _other.m_x;
	m_y += _other.m_y;
	m_z += _other.m_z;
}

void Vec3::operator-=(const Vec3& _other)
{
	m_x -= _other.m_x;
	m_y -= _other.m_y;
	m_z -= _other.m_z;
}

Vec3 operator+(const Vec3& _a, const Vec3& _b)
{
	return Vec3(_a.m_x + _b.m_x, _a.m_y + _b.m_y, _a.m_z + _b.m_z);
}

Vec3 operator-(const Vec3& _a, const Vec3& _b)
{
	return Vec3(_a.m_x - _b.m_x, _a.m_y - _b.m_y, _a.m_z - _b.m_z);
}

Vec3 operator*(const Vec3& _a, double _b)
{
	return Vec3(_a.m_x * _b, _a.m_y * _b, _a.m_z * _b);
}

Vec3 operator/(const Vec3& _a, double _b)
{
	return Vec3(_a.m_x / _b, _a.m_y / _b, _a.m_z / _b);
}

bool operator==(const Vec3& _a, const Vec3& _b)
{
	return _a.m_x == _b.m_x && _a.m_y == _b.m_y && _a.m_z == _b.m_z;
}

std::ostream& operator<<(std::ostream& _stream, const Vec3& _o)
{
	return _stream << "(" << _o.m_x << ";" << _o.m_y << ";" << _o.m_z << ")";
}
