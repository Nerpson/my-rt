#include "MissingXmlNodeException.h"
#include <sstream>

MissingXmlNodeException::MissingXmlNodeException(const std::string& _missing_xml_node_name) noexcept
	: MalformedXmlException(""), m_missing_xml_node_name(_missing_xml_node_name)
{
	std::stringstream ss;
	ss << "Missing \"" << m_missing_xml_node_name << "\" node.";
	m_what = ss.str();
}
