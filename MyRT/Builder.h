#pragma once
#include <memory>
#include <map>
#include <vector>
#include "Scene.h"
#include "Object.h"
#include "Light.h"
#include "Camera.h"

namespace Scene
{
	class Builder
	{
		Color m_backdrop_color = Color(0., 0., 0.);
		std::unique_ptr<std::map<std::string, std::unique_ptr<Material>>> m_materials;
		std::unique_ptr<std::vector<std::unique_ptr<Object>>> m_objects;
		std::unique_ptr<std::vector<std::unique_ptr<Light>>> m_lights;
		std::unique_ptr<Camera> m_camera;

	public:
		Builder();

		/**
		 * Gets the registered materials.
		 * @return The registered materials map.
		 */
		const std::map<std::string, std::unique_ptr<Material>>& materials() const;

		/**
		 * Sets the backdrop color.
		 * @param _backdrop_color The color of the backdrop.
		 */
		void set_backdrop_color(const Color& _backdrop_color);

		/**
		 * Adds a material to the scene.
		 * @param _material The material to add.
		 */
		void add_material(std::unique_ptr<Material>& _material);

		/**
		 * Adds an object to the scene.
		 * @param _object The object to add.
		 */
		void add_object(std::unique_ptr<Object>& _object);

		/**
		 * Adds a light to the scene.
		 * @param _light The light to add.
		 */
		void add_light(std::unique_ptr<Light>& _light);

		/**
		 * Sets the scene camera to use.
		 * @param _camera The camera to set.
		 */
		void set_camera(std::unique_ptr<Camera>& _camera);
		
		/**
		 * Gets the created scene.
		 * @return The unique pointer to the scene.
		 */
		std::unique_ptr<Scene> get();
	};

	inline void Builder::set_backdrop_color(const Color& _backdrop_color)
	{
		m_backdrop_color = _backdrop_color;
	}

	inline const std::map<std::string, std::unique_ptr<Material>>& Builder::materials() const
	{
		return *m_materials;
	}
}
