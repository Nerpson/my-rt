#include "Color.h"
#include <regex>
#include <cctype>
#include <algorithm>

Color Color::to_ranged(double _lower_bound, double _upper_bound) const
{
	return Color(
		std::clamp(m_r, _lower_bound, _upper_bound),
		std::clamp(m_g, _lower_bound, _upper_bound),
		std::clamp(m_b, _lower_bound, _upper_bound)
	);
}

double hex_to_double(char _hex_char)
{
	switch (_hex_char)
	{
	case 48: return 0.f;
	case 49: return 1.f;
	case 50: return 2.f;
	case 51: return 3.f;
	case 52: return 4.f;
	case 53: return 5.f;
	case 54: return 6.f;
	case 55: return 7.f;
	case 56: return 8.f;
	case 57: return 9.f;
	case 65:
	case 97: return 10.f;
	case 66:
	case 98: return 11.f;
	case 67:
	case 99: return 12.f;
	case 68:
	case 100: return 13.f;
	case 69:
	case 101: return 14.f;
	case 70:
	case 102: return 15.f;
	}

	throw std::runtime_error("Unknown character.");
}

Color Color::from_web(const char* _color_string)
{
	std::string color_string(_color_string);
	std::regex regex("^#(?:[0-9a-f]{3}|[0-9a-f]{6})$", std::regex_constants::syntax_option_type::icase);
	bool matches = std::regex_match(color_string, regex);
	if (!matches) throw std::runtime_error("Color is not valid");

	double colors[3] = { 0.f, 0.f, 0.f };
	if (color_string.size() == 4)
	{
		for (unsigned i = 0; i < 3; ++i) colors[i] = hex_to_double(color_string[1u + i]) * 17.f;
	}
	else
	{
		for (unsigned i = 0; i < 3; ++i)
		{
			colors[i] = hex_to_double(color_string[1u + i * 2u]) * 16.f + hex_to_double(color_string[2u + i * 2u]);
		}
	}

	return Color(colors[0] / 255.f, colors[1] / 255.f, colors[2] / 255.f);
}

void Color::operator=(const Color& _other)
{
	m_r = _other.m_r;
	m_g = _other.m_g;
	m_b = _other.m_b;
}

void Color::operator+=(const Color& _other)
{
	m_r += _other.m_r;
	m_g += _other.m_g;
	m_b += _other.m_b;
}

void Color::operator-=(const Color& _other)
{
	m_r -= _other.m_r;
	m_g -= _other.m_g;
	m_b -= _other.m_b;
}

Color operator+(const Color& _a, const Color& _b)
{
	return Color(_a.m_r + _b.m_r, _a.m_g + _b.m_g, _a.m_b + _b.m_b);
}

Color operator*(const Color& _a, const Color& _b)
{
	return Color(_a.m_r * _b.m_r, _a.m_g * _b.m_g, _a.m_b * _b.m_b);
}

Color operator*(const Color& _a, double _b)
{
	return Color(_a.m_r * _b, _a.m_g * _b, _a.m_b * _b);
}

Color operator/(const Color& _a, double _b)
{
	return Color(_a.m_r / _b, _a.m_g / _b, _a.m_b / _b);
}

bool operator==(const Color& _a, const Color& _b)
{
	return _a.m_r == _b.m_r &&
		_a.m_g == _b.m_g &&
		_a.m_b == _b.m_b;
}

std::ostream& operator<<(std::ostream& _stream, const Color& _o)
{
	return _stream << '(' << _o.m_r << ',' << _o.m_g << ',' << _o.m_b << ')';
}
