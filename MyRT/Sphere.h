#pragma once
#include "Object.h"

namespace Scene
{
	class Sphere : public Object
	{
		double m_radius = 1.;

	public:
		Sphere();

		virtual bool intersects(Ray::Ray _ray, Vec3& _intersection, Vec3& _normal) const override;

		/**
		 * Gets the sphere's radius.
		 * @return The sphere's radius.
		 */
		double radius() const;

		/**
		 * Sets the sphere's radius.
		 * @param _radius The radius to set.
		 */
		void set_radius(double _radius);
	};

	inline Sphere::Sphere() : Object(EObjectType::Sphere) {}

	inline double Sphere::radius() const
	{
		return m_radius;
	}

	inline void Sphere::set_radius(double _radius)
	{
		m_radius = _radius;
	}
}
