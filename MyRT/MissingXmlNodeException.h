#pragma once
#include "MalformedXmlException.h"

class MissingXmlNodeException final : public MalformedXmlException
{
	std::string m_missing_xml_node_name;
	std::string m_what;

public:
	explicit MissingXmlNodeException(const std::string& _missing_xml_node_name) noexcept;
	const char* what() const noexcept override;

	const char* missing_xml_node_name() const noexcept;
};

inline const char* MissingXmlNodeException::what() const noexcept
{
	return m_what.c_str();
}

inline const char* MissingXmlNodeException::missing_xml_node_name() const noexcept
{
	return m_missing_xml_node_name.c_str();
}
