#pragma once
class Angle
{
	double m_radians;
	double m_degrees;

	Angle(double _radians, double _degrees);

public:
	/**
	 * Creates a void Angle.
	 */
	Angle();

	/**
	 * Creates an Angle object from a radian value.
	 * @param _radians The angle value in radians.
	 * @return The Angle instance.
	 */
	static Angle from_radians(double _radians);

	/**
	 * Creates an Angle object from a degree value.
	 * @param _degrees The angle value in degrees.
	 * @return The Angle instance.
	 */
	static Angle from_degrees(double _degrees);

	/**
	 * Gets the Angle value in radians.
	 * @return The radians value.
	 */
	double radians() const;
	
	/**
	 * Gets the Angle value in degrees.
	 * @return The degrees value.
	 */
	double degrees() const;
};

inline Angle::Angle(double _radians, double _degrees) : m_radians(_radians), m_degrees(_degrees) {}
inline Angle::Angle() : Angle(0.f, 0.f) {}

inline double Angle::radians() const
{
	return m_radians;
}

inline double Angle::degrees() const
{
	return m_degrees;
}
