#include "Tracer.h"
#include <iostream>
#include <limits>
#include <numbers>
#include "Camera.h"

namespace Ray
{
	Color Tracer::cast_ray(const Ray& _ray, unsigned _depth)
	{
		if (_depth > m_max_depth) return m_scene.backdrop_color();

		double min_distance = std::numeric_limits<double>::infinity();
		const Scene::Object* intersected_object = nullptr;
		Vec3 intersection;
		Vec3 intersection_normal;

		Vec3 candidate_intersection;
		Vec3 candidate_normal;
		for (const std::unique_ptr<Scene::Object>& candidate_object : m_scene.objects())
		{
			if (candidate_object->intersects(_ray, candidate_intersection, candidate_normal))
			{
				double candidate_distance = Vec3::distance(_ray.origin, candidate_intersection);
				if (candidate_distance < min_distance)
				{
					min_distance = candidate_distance;
					intersected_object = candidate_object.get();
					intersection = candidate_intersection;
					intersection_normal = candidate_normal;
				}
			}
		}

		Color final_color;

		if (intersected_object == nullptr)
		{
			final_color = m_scene.backdrop_color();
		}
		else
		{
			for (auto& light : m_scene.lights())
			{
				Color added_colors(0., 0., 0.);
				const unsigned samples = (light->radius() == 0.) ? 1u : light->samples();

				for (unsigned i = 0; i < light->samples(); ++i)
				{
					Vec3 direction = light->position() - intersection;
					if (samples > 1) direction += Vec3::random_filled(-light->radius(), light->radius());
					Ray light_ray(intersection, direction.to_unit());

					bool shaded(false);
					Vec3 light_ray_intersection;
					Vec3 light_ray_intersection_normal;
					for (auto& object : m_scene.objects())
					{
						if (
							object->intersects(light_ray, light_ray_intersection, light_ray_intersection_normal)
							)
						{
							shaded = true;
							break;
						}
					}

					if (!shaded) added_colors += light->color();
				}

				const double distance = Vec3::distance(intersection, light->position());
				const Color received_light = (added_colors / samples * light->intensity()) / (4. * std::numbers::pi_v<double> *pow(distance, 2.));
				final_color += intersected_object->material().diffuse() * received_light;
			}

			Vec3 reflection_direction(_ray.direction - 2. * Vec3::dot(intersection_normal, _ray.direction) * intersection_normal);
			Ray reflection_ray(intersection, reflection_direction);

			const double specular = intersected_object->material().specular();
			if (specular > 0) final_color += intersected_object->material().diffuse() * specular * cast_ray(reflection_ray, _depth + 1u);
		}

		return final_color;
	}

	void Tracer::compute_tile(const Tile& _tile)
	{
		const Scene::Camera& camera = m_scene.camera();
		const unsigned image_width = m_image_sharer.image_width();
		const unsigned image_height = m_image_sharer.image_height();

		constexpr double canvas_distance_z = 1.;

		const double fov = camera.fov().radians();
		const Vec3& camera_origin = camera.position();

		double canvas_width = tan(fov / 2.) * 2.;
		double canvas_height = canvas_width * image_height / image_width;
		double canvas_half_width = canvas_width / 2.;
		double canvas_half_height = canvas_height / 2.;

		double px_half_size = canvas_width / image_width / 2.;

		for (unsigned x = _tile.from_x(); x <= _tile.to_x(); ++x)
		{
			for (unsigned y = _tile.from_y(); y <= _tile.to_y(); ++y)
			{
				Vec3 canvas_px_center = camera_origin + (Vec3::forward() + Vec3(
					-canvas_half_width + static_cast<double>(x) * canvas_width / image_width + px_half_size,
					-canvas_half_height + static_cast<double>(y) * canvas_height / image_height + px_half_size,
					0.
				)).rotated(camera.yaw(), camera.pitch(), camera.roll());
				Ray primary_ray(camera_origin, (canvas_px_center - camera_origin).to_unit());

				Color final_px_color = cast_ray(primary_ray);

				m_image_sharer.put_color(x, y, final_px_color.to_ranged(0., 1.));
			}
		}
	}

	void Tracer::operator()()
	{
		while (true)
		{
			Tile* next_tile = m_tile_queue.next();
			if (!next_tile) break;
			compute_tile(*next_tile);
		}
	}
}