#pragma once
class Random
{
	static unsigned int time_since_midnight();

public:
	static double get(double _lower_bound, double _upper_bound);
};
