#include <iostream>
#include "MalformedXmlException.h"
#include "MissingXmlNodeException.h"
#include "MissingXmlAttributeException.h"
#include "Loader.h"
#include "Camera.h"
#include "Light.h"
#include "Plane.h"
#include "Sphere.h"

Loader::Loader(const char* _file_path) : m_file_path(_file_path)
{
	m_doc.LoadFile(_file_path);
		
	if (m_doc.Error()) throw MalformedXmlException("The file is invalid.");

	const tinyxml2::XMLElement* root = m_doc.FirstChildElement();

	if (root == nullptr) throw MalformedXmlException("No XML root found.");

	if (strncmp(root->Name(), "configuration", 6) != 0) throw MalformedXmlException("The root must be a \"configuration\" node.");

	const tinyxml2::XMLAttribute* threads_attr = root->FindAttribute("threads");
	if (threads_attr == nullptr) throw MissingXmlAttributeException("configuration", "threads");
	m_thread_count = threads_attr->UnsignedValue();

	const tinyxml2::XMLAttribute* bounces_attr = root->FindAttribute("bounces");
	if (bounces_attr == nullptr) throw MissingXmlAttributeException("configuration", "bounces");
	m_bounce_count = bounces_attr->UnsignedValue();

	// Output.
	const tinyxml2::XMLElement* output_node = root->FirstChildElement("output");
	if (output_node == nullptr) throw MissingXmlNodeException("output");

	const tinyxml2::XMLAttribute* width_attr = output_node->FindAttribute("width");
	if (width_attr == nullptr) throw MissingXmlAttributeException("output", "width");
	m_image_width = width_attr->UnsignedValue();

	const tinyxml2::XMLAttribute* height_attr = output_node->FindAttribute("height");
	if (height_attr == nullptr) throw MissingXmlAttributeException("output", "height");
	m_image_height = height_attr->UnsignedValue();

	const tinyxml2::XMLAttribute* output_file_attr = output_node->FindAttribute("file");
	if (output_file_attr == nullptr) throw MissingXmlAttributeException("output", "file");
	m_output_file = output_file_attr->Value();

	// Scene.
	const tinyxml2::XMLElement* scene_node = root->FirstChildElement("scene");
	if (scene_node == nullptr) throw MissingXmlNodeException("scene");

	const tinyxml2::XMLAttribute* backdrop_attr = scene_node->FindAttribute("backdrop");
	if (backdrop_attr != nullptr) m_builder.set_backdrop_color(Color::from_web(backdrop_attr->Value()));

	// Materials.
	const tinyxml2::XMLElement* materials_node = scene_node->FirstChildElement("materials");
	if (materials_node == nullptr) throw MissingXmlNodeException("materials");

	const tinyxml2::XMLElement* material_node = materials_node->FirstChildElement();
	while (material_node != nullptr)
	{
		if (strncmp(material_node->Name(), "material", 9) != 0)
		{
			throw MalformedXmlException("The children of \"materials\" node must all be \"material\" nodes.");
		}

		const tinyxml2::XMLAttribute* id_attr = material_node->FindAttribute("id");
		if (id_attr == nullptr) throw MissingXmlAttributeException("material", "id");
		const char* material_id = id_attr->Value();

		const tinyxml2::XMLAttribute* diffuse_attr = material_node->FindAttribute("diffuse");
		if (diffuse_attr == nullptr) throw MissingXmlAttributeException("material", "diffuse");
		Color material_diffuse = Color::from_web(diffuse_attr->Value());

		double material_specular(0.);
		const tinyxml2::XMLAttribute* specular_attr = material_node->FindAttribute("specular");
		if (specular_attr != nullptr) material_specular = specular_attr->DoubleValue();

		std::unique_ptr<Scene::Material> material = std::make_unique<Scene::Material>(material_id, material_diffuse, material_specular);
		std::clog << "Adding material \"" << material_id << "\" with diffuse color " << material_diffuse << '.' << std::endl;
		m_builder.add_material(material);

		material_node = material_node->NextSiblingElement();
	}

	// Camera.
	const tinyxml2::XMLElement* camera_node = scene_node->FirstChildElement("camera");
	if (camera_node == nullptr) throw MissingXmlNodeException("camera");
	std::clog << "Adding camera." << std::endl;

	{
		std::unique_ptr<Scene::Camera> camera = std::make_unique<Scene::Camera>();

		const tinyxml2::XMLAttribute* pos_attr = camera_node->FindAttribute("pos");
		if (pos_attr == nullptr) throw MissingXmlAttributeException("camera", "pos");
		camera->set_position(Vec3::from_string(pos_attr->Value()));

		const tinyxml2::XMLAttribute* yaw_attr = camera_node->FindAttribute("yaw");
		if (yaw_attr != nullptr) camera->set_yaw(Angle::from_degrees(yaw_attr->DoubleValue()));

		const tinyxml2::XMLAttribute* pitch_attr = camera_node->FindAttribute("pitch");
		if (pitch_attr != nullptr) camera->set_pitch(Angle::from_degrees(pitch_attr->DoubleValue()));

		const tinyxml2::XMLAttribute* roll_attr = camera_node->FindAttribute("roll");
		if (roll_attr != nullptr) camera->set_roll(Angle::from_degrees(roll_attr->DoubleValue()));

		const tinyxml2::XMLAttribute* fov_attr = camera_node->FindAttribute("fov");
		if (fov_attr != nullptr) camera->set_fov(Angle::from_degrees(fov_attr->DoubleValue()));

		m_builder.set_camera(camera);
	}

	// Lights.
	const tinyxml2::XMLElement* lights_node = scene_node->FirstChildElement("lights");
	if (lights_node != nullptr)
	{
		const tinyxml2::XMLElement* light_node = lights_node->FirstChildElement();
		while (light_node != nullptr)
		{
			if (strncmp(light_node->Name(), "light", 6) != 0)
			{
				throw MalformedXmlException("The children of \"lights\" node must all be \"light\" nodes.");
			}

			std::string id("unnamed_light");
			const tinyxml2::XMLAttribute* id_attr = light_node->FindAttribute("id");
			if (id_attr != nullptr) id = id_attr->Value();

			const tinyxml2::XMLAttribute* pos_attr = light_node->FindAttribute("pos");
			if (pos_attr == nullptr) throw MissingXmlAttributeException("light", "pos");
			Vec3 position = Vec3::from_string(pos_attr->Value());

			const tinyxml2::XMLAttribute* color_attr = light_node->FindAttribute("color");
			if (color_attr == nullptr) throw MissingXmlAttributeException("light", "color");
			Color color = Color::from_web(color_attr->Value());

			const tinyxml2::XMLAttribute* intensity_attr = light_node->FindAttribute("intensity");
			if (intensity_attr == nullptr) throw MissingXmlAttributeException("light", "intensity");
			double intensity = intensity_attr->DoubleValue();

			const tinyxml2::XMLAttribute* radius_attr = light_node->FindAttribute("radius");
			if (radius_attr == nullptr) throw MissingXmlAttributeException("light", "radius");
			double radius = radius_attr->DoubleValue();

			const tinyxml2::XMLAttribute* samples_attr = light_node->FindAttribute("samples");
			if (samples_attr == nullptr) throw MissingXmlAttributeException("light", "samples");
			unsigned samples = samples_attr->UnsignedValue();

			std::unique_ptr<Scene::Light> light = std::make_unique<Scene::Light>(id, position, color, intensity, radius, samples);
			m_builder.add_light(light);

			light_node = light_node->NextSiblingElement();
		}
	}

	// Objects.
	const tinyxml2::XMLElement* objects_node = scene_node->FirstChildElement("objects");
	if (objects_node == nullptr) throw MissingXmlNodeException("objects");

	const tinyxml2::XMLElement* object_node = objects_node->FirstChildElement();
	while (object_node != nullptr)
	{
		const char* object_type = object_node->Name();
		std::unique_ptr<Scene::Object> object;
		std::string id;
		if (strncmp(object_type, "plane", 6) == 0)
		{
			std::clog << "Adding plane." << std::endl;
			id = "unnamed_plane";

			std::unique_ptr<Scene::Plane> plane = std::make_unique<Scene::Plane>();
			try_add_material(object_node, *plane);
			object = std::move(plane);
		}
		else if (strncmp(object_type, "sphere", 7) == 0)
		{
			std::clog << "Adding sphere." << std::endl;
			id = "unnamed_sphere";

			std::unique_ptr<Scene::Sphere> sphere = std::make_unique<Scene::Sphere>();
			try_add_material(object_node, *sphere);

			const tinyxml2::XMLAttribute* radius_attr = object_node->FindAttribute("radius");
			if (radius_attr != nullptr) sphere->set_radius(radius_attr->DoubleValue());

			object = std::move(sphere);
		}
		else
		{
			throw MalformedXmlException("The children of \"objects\" node must all be \"object\" nodes.");
		}

		// Id.
		const tinyxml2::XMLAttribute* id_attr = object_node->FindAttribute("id");
		if (id_attr != nullptr) id = id_attr->Value();
		object->set_id(id);

		// Position.
		const tinyxml2::XMLAttribute* pos_attr = object_node->FindAttribute("pos");
		if (pos_attr == nullptr) throw MalformedXmlException("Missing \"pos\" attribute on object node.");
		object->set_position(Vec3::from_string(pos_attr->Value()));

		// Visibility.
		const tinyxml2::XMLAttribute* hidden_attr = object_node->FindAttribute("hidden");
		if (hidden_attr != nullptr) object->set_hidden(hidden_attr->BoolValue());

		// Orientation.
		const tinyxml2::XMLAttribute* pitch_attr = object_node->FindAttribute("pitch");
		if (pitch_attr != nullptr) object->set_pitch(Angle::from_degrees(pitch_attr->DoubleValue()));

		const tinyxml2::XMLAttribute* roll_attr = object_node->FindAttribute("roll");
		if (roll_attr != nullptr) object->set_roll(Angle::from_degrees(roll_attr->DoubleValue()));

		const tinyxml2::XMLAttribute* yaw_attr = object_node->FindAttribute("yaw");
		if (yaw_attr != nullptr) object->set_yaw(Angle::from_degrees(yaw_attr->DoubleValue()));

		m_builder.add_object(object);
		object_node = object_node->NextSiblingElement();
	}
}

void Loader::try_add_material(const tinyxml2::XMLElement* _node, Scene::Object& _object)
{
	const tinyxml2::XMLAttribute* material_attr = _node->FindAttribute("material");
	if (material_attr == nullptr) throw MalformedXmlException("Missing \"material\" attribute on object.");
	auto it = m_builder.materials().find(material_attr->Value());
	if (it == m_builder.materials().end()) throw MalformedXmlException("An object is using an unknown material id.");
	_object.set_material(it->second.get());
}

std::unique_ptr<Scene::Scene> Loader::make_scene()
{
	return m_builder.get();
}
