#include "EObjectType.h"

namespace Scene
{
	const char* object_type_to_string(const EObjectType& _object_type)
	{
		switch (_object_type)
		{
		case EObjectType::Plane:
			return "Plane";
		case EObjectType::Sphere:
			return "Sphere";
		}
		throw std::invalid_argument("Unknown object type.");
	}

	bool object_type_exists(const char* _object_type)
	{
		return strncmp(_object_type, "plane", 6) == 0 || strncmp(_object_type, "sphere", 7) == 0;
	}
}
