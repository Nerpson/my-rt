#pragma once
#include "Vec3.h"
#include "Ray.h"
#include "EObjectType.h"
#include "Material.h"
#include "Angle.h"

namespace Scene
{
	class Object
	{
		EObjectType m_object_type;

		const Material* m_material = nullptr;

		std::string m_id;
		Vec3 m_position = Vec3(0., 0., 0.);
		bool m_hidden = false;
		Angle m_pitch;
		Angle m_roll;
		Angle m_yaw;

	protected:
		Object(EObjectType _object_type);

	public:
		virtual ~Object() = default;

		/**
		 * Checks if an intersection exists with the given ray.
		 * @param _ray The ray to check.
		 * @param _intersection The position object to write the intersection to, if existing.
		 * @return True if the ray intersects the object, False otherwise.
		 */
		virtual bool intersects(Ray::Ray _ray, Vec3& _intersection, Vec3& _normal) const = 0;

		/**
		 * Gets the object's material.
		 * @return The object's material.
		 */
		const Material& material() const;
		
		/**
		 * Gets the object's identifier string.
		 * @return The object's id.
		 */
		const std::string& id() const;

		/**
		 * Gets the object's position.
		 * @return The object's position.
		 */
		const Vec3& position() const;
		
		/**
		 * Gets the object's visibility.
		 * @return True if the object is hidden, False if the object is visible.
		 */
		bool hidden() const;

		/**
		 * Gets the object's pitch angle.
		 * @return The object's pitch.
		 */
		const Angle& pitch() const;
		
		/**
		 * Gets the object's roll angle.
		 * @return The object's roll.
		 */
		const Angle& roll() const;
		
		/**
		 * Gets the object's yaw angle.
		 * @return The object's yaw.
		 */
		const Angle& yaw() const;

		/**
		 * Sets the object's id.
		 * @param _string The id to set.
		 */
		void set_id(const std::string& _string);

		/**
		 * Sets the object's material.
		 * @param _material The material to set.
		 */
		void set_material(const Material* _material);

		/**
		 * Sets the object's position.
		 * @param _position The position to set.
		 */
		void set_position(const Vec3& _position);

		/**
		 * Sets the object's visibility.
		 * @param _hidden True to hide the object, False to show it.
		 */
		void set_hidden(bool _hidden);

		/**
		 * Sets the object's pitch angle.
		 * @param _pitch The pitch to set.
		 */
		void set_pitch(const Angle& _pitch);

		/**
		 * Sets the object's roll angle.
		 * @param _roll The roll to set.
		 */
		void set_roll(const Angle& _roll);

		/**
		 * Sets the object's yaw angle.
		 * @param _yaw The yaw to set.
		 */
		void set_yaw(const Angle& _yaw);
	};

	inline Object::Object(EObjectType _object_type) : m_object_type(_object_type) {}

	inline const Material& Object::material() const
	{
		return *m_material;
	}

	inline const std::string& Object::id() const
	{
		return m_id;
	}

	inline const Vec3& Object::position() const
	{
		return m_position;
	}

	inline bool Object::hidden() const
	{
		return m_hidden;
	}

	inline const Angle& Object::pitch() const
	{
		return m_pitch;
	}

	inline const Angle& Object::roll() const
	{
		return m_roll;
	}

	inline const Angle& Object::yaw() const
	{
		return m_yaw;
	}

	inline void Object::set_material(const Material* _material)
	{
		m_material = _material;
	}

	inline void Object::set_id(const std::string& _id)
	{
		m_id = _id;
	}

	inline void Object::set_position(const Vec3& _position)
	{
		m_position = _position;
	}

	inline void Object::set_hidden(bool _hidden)
	{
		m_hidden = _hidden;
	}

	inline void Object::set_pitch(const Angle& _pitch)
	{
		m_pitch = _pitch;
	}

	inline void Object::set_roll(const Angle& _roll)
	{
		m_roll = _roll;
	}

	inline void Object::set_yaw(const Angle& _yaw)
	{
		m_yaw = _yaw;
	}

}
