#pragma once
#include <vector>
#include <string>

class Binary
{
	std::vector<char> m_binary;

public:
	/**
	 * Clears the whole data.
	 */
	void clear();

	/**
	 * Gets the size in bytes.
	 */
	size_t size() const;

	/**
	 * Adds the given byte.
	 * @param _byte The byte to add.
	 */
	void add_byte(char _byte);

	/**
	 * Adds the given byte a specified amount of times.
	 * @param _byte The byte to add.
	 * @param _times The number of times to add the byte.
	 */
	void add_byte(char _byte, unsigned _times);

	/**
	 * Adds the given bytes.
	 * @param _bytes The bytes to add.
	 * @param _size The size of the {@code _bytes} array.
	 */
	void add_bytes(const char* _bytes, unsigned _size);

	/**
	 * Overwrites an existing byte.
	 * @param _offset The position of the byte to write in the binary.
	 * @param _byte The byte to write.
	 */
	void set_byte(unsigned _offset, char byte);

	/**
	 * Adds 4 bytes representing the given integer.
	 * @param _integer The integer to add.
	 */
	void add_integer(int _integer);

	/**
	 * Overwrites 4 existing bytes with the given integer.
	 * @param _offset The position of the integer to write in the binary.
	 * @param _integer The integer to write.
	 */
	void set_integer(unsigned _offset, int _integer);

	/**
	 * Adds 2 bytes representing the given integer.
	 * @param _short_integer The short integer to add.
	 */
	void add_short_integer(short _short_integer);

	/**
	 * Gets the vector containing all bytes.
	 * @return The vector of bytes.
	 */
	const std::vector<char>& get_vector() const;
};

inline void Binary::clear()
{
	m_binary.clear();
}

inline size_t Binary::size() const
{
	return m_binary.size();
}

inline const std::vector<char>& Binary::get_vector() const
{
	return m_binary;
}

/**
 * Writes a binary into a file.
 */
void write_to_file(const std::string& _filename, const Binary& _binary);