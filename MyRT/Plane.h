#pragma once
#include "Object.h"

namespace Scene
{
	class Plane : public Object
	{
	public:
		Plane();

		virtual bool intersects(Ray::Ray _ray, Vec3& _intersection, Vec3& _normal) const override;
	};

	inline Plane::Plane() : Object(EObjectType::Plane) {}
}
