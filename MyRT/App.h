#pragma once
#include <memory>
#include "Loader.h"
#include "Image.h"
#include "Scene.h"

class App
{
	static constexpr const char* s_config_file_path = "./config.xml";
	static constexpr unsigned s_tile_size = 128u;

	std::unique_ptr<Loader> m_loader;
	std::unique_ptr<Image> m_image;
	std::unique_ptr<Scene::Scene> m_scene;

public:
	/**
	 * Initializes the application.
	 */
	App(unsigned _argc, const char* _argv[]);

	/**
	 * Starts the rendering process.
	 */
	void run();
};

