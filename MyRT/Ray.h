#pragma once
#include "Vec3.h"
#define MIN_INTERSECTION_DISTANCE 0.000001

namespace Ray
{
	struct Ray
	{
		Vec3 origin;
		Vec3 direction;

		/**
		 * Creates a ray at an origin and with a direction.
		 * @param _origin The ray's origin.
		 * @param _direction The ray's direction.
		 */
		Ray(const Vec3& _origin, const Vec3& _direction);
	};

	inline Ray::Ray(const Vec3& _origin, const Vec3& _direction) :
		origin(_origin), direction(_direction)
	{}
}
