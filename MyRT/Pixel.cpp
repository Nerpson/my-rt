#include "Pixel.h"

void Pixel::set_color(const Color& _color)
{
	r = _color.r();
	g = _color.g();
	b = _color.b();
}