#include "Angle.h"
#include <numbers>

Angle Angle::from_radians(double _radians)
{
	return Angle(
		_radians,
		_radians / std::numbers::pi_v<double> * 180.f
	);
}

Angle Angle::from_degrees(double _degrees)
{
	return Angle(
		_degrees / 180.f * std::numbers::pi_v<double>,
		_degrees
	);
}
