#pragma once
#include "Vec3.h"
#include "Color.h"

namespace Scene
{
	class Light
	{
		const std::string m_id;
		const Vec3 m_position;
		const Color m_color;
		const double m_intensity;
		const double m_radius;
		const unsigned m_samples;

	public:
		/**
		 * Creates a light.
		 * @param _position The light's position.
		 * @param _color The light's color.
		 * @param _intensity The light's intensity.
		 * @param _radius The light's radius.
		 * @param _samples The number of samples to be sent onto this light.
		 */
		Light(const std::string& _light, const Vec3& _position, const Color& _color, double _intensity, double _radius, unsigned _samples);

		/**
		 * Gets the light's identifier string.
		 * @return The light's id.
		 */
		const std::string& id() const;

		/**
		 * Gets the light's position.
		 * @return The light's position.
		 */
		const Vec3& position() const;

		/**
		 * Gets the light's color.
		 * @return The light's color.
		 */
		const Color& color() const;

		/**
		 * Gets the light's intensity.
		 * @return The light's intensity.
		 */
		double intensity() const;

		/**
		 * Gets the light's radius.
		 * @return The light's radius.
		 */
		double radius() const;

		/**
		 * Gets the light's samples count.
		 * @return The light's samples count.
		 */
		unsigned samples() const;
	};

	inline Light::Light(const std::string& _id, const Vec3& _position, const Color& _color, double _intensity, double _radius, unsigned _samples) :
		m_id(_id), m_position(_position), m_color(_color), m_intensity(_intensity), m_radius(_radius), m_samples(_samples)
	{}

	inline const std::string& Light::id() const
	{
		return m_id;
	}

	inline const Vec3& Light::position() const
	{
		return m_position;
	}

	inline const Color& Light::color() const
	{
		return m_color;
	}

	inline double Light::intensity() const
	{
		return m_intensity;
	}

	inline double Light::radius() const
	{
		return m_radius;
	}

	inline unsigned Light::samples() const
	{
		return m_samples;
	}
}
