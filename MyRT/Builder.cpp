#include "Builder.h"

namespace Scene
{
	Builder::Builder() :
		m_materials(std::make_unique<std::map<std::string, std::unique_ptr<Material>>>()),
		m_objects(std::make_unique<std::vector<std::unique_ptr<Object>>>()),
		m_lights(std::make_unique<std::vector<std::unique_ptr<Light>>>())
	{}

	void Builder::add_material(std::unique_ptr<Material>& _material)
	{
		m_materials->insert(std::make_pair(_material->id(), std::move(_material)));
	}

	void Builder::add_object(std::unique_ptr<Object>& _object)
	{
		m_objects->push_back(std::move(_object));
	}

	void Builder::add_light(std::unique_ptr<Light>& _light)
	{
		m_lights->push_back(std::move(_light));
	}

	void Builder::set_camera(std::unique_ptr<Camera>& _camera)
	{
		m_camera = std::move(_camera);
	}

	std::unique_ptr<Scene> Builder::get()
	{
		return std::make_unique<Scene>(m_backdrop_color, m_materials, m_objects, m_lights, m_camera);
	}
}
