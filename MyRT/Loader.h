#pragma once
#include <memory>
#pragma warning(push, 0)
	#include "tinyxml2.h"
#pragma warning(pop)
#include "Scene.h"
#include "Builder.h"

class Loader
{
	std::string m_file_path;
	tinyxml2::XMLDocument m_doc;
	Scene::Builder m_builder;
	std::unique_ptr<Scene::Scene> m_scene;

	unsigned m_thread_count;
	unsigned m_bounce_count;
	unsigned m_image_width;
	unsigned m_image_height;
	std::string m_output_file;

	void try_add_material(const tinyxml2::XMLElement* _node, Scene::Object& _object);
public:
	/**
	 * Loads the XML file located at the given path.
	 * @param _file_path The path to the XML file to load.
	 */
	Loader(const char* _file_path);

	/**
	 * Creates and returns the scene.
	 */
	std::unique_ptr<Scene::Scene> make_scene();

	/**
	 * Returns the maximum number of bounces to perform.
	 * @return The number of bounces.
	 */
	unsigned bounce_count() const;

	/**
	 * Returns the number of threads to use to render.
	 * @return The number of threads.
	 */
	unsigned thread_count() const;

	/**
	 * The image's width.
	 * @return The image's width.
	 */
	unsigned image_width() const;

	/**
	 * The image's height.
	 * @return The image's height.
	 */
	unsigned image_height() const;

	/**
	 * The output file's path.
	 * @return The output file's path.
	 */
	const std::string& output_file() const;
};

inline unsigned Loader::thread_count() const
{
	return m_thread_count;
}

inline unsigned Loader::bounce_count() const
{
	return m_bounce_count;
}

inline unsigned Loader::image_width() const
{
	return m_image_width;
}

inline unsigned Loader::image_height() const
{
	return m_image_height;
}

inline const std::string& Loader::output_file() const
{
	return m_output_file;
}
