#include "Quaternion.h"
#include <cmath>

Quaternion Quaternion::from_angles(const Angle& _yaw, const Angle& _pitch, const Angle& _roll)
{
	double cy = cos(_yaw.radians() * 0.5);
	double sy = sin(_yaw.radians() * 0.5);
	double cp = cos(_pitch.radians() * 0.5);
	double sp = sin(_pitch.radians() * 0.5);
	double cr = cos(_roll.radians() * 0.5);
	double sr = sin(_roll.radians() * 0.5);

	double crcp = cr * cp;
	double srsp = sr * sp;
	double crsp = cr * sp;
	double srcp = sr * cp;

	return Quaternion {
		crcp * cy + srsp * sy,
		crsp * cy + srcp * sy,
		crcp * sy - srsp * cy,
		srcp * cy - crsp * sy
	};
}

Vec3 Quaternion::rotate(const Vec3& _vec3) const
{
	Vec3 u(x, y, z);
	return 2. * Vec3::dot(u, _vec3) * u +
		(pow(w, 2.) - Vec3::dot(u, u)) * _vec3 +
		2. * w * Vec3::cross(u, _vec3);
}