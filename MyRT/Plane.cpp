#include "Plane.h"

namespace Scene
{
	bool Plane::intersects(Ray::Ray _ray, Vec3& _intersection, Vec3& _normal) const
	{
		_normal = Vec3(0., 1., 0.).rotated(yaw(), pitch(), roll());
		double denominator = Vec3::dot(_ray.direction, _normal);
		if (denominator == 0) return false;

		double numerator = Vec3::dot(position() - _ray.origin, _normal);
		double intersection_distance = numerator / denominator;
		if (intersection_distance <= MIN_INTERSECTION_DISTANCE) return false;

		_intersection = _ray.origin + _ray.direction * intersection_distance;
		return true;
	}
}