#pragma once
#include "Image.h"
#include "Binary.h"

/**
 * Converts an image to a binary object.
 * @param _img The image to get the data from.
 * @param _binary The binary to write to.
 */
void img_to_bitmap(const Image* _img, Binary& _binary);