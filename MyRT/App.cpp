#include "App.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <thread>
#include "Timer.h"
#include "Tracer.h"
#include "TileQueue.h"
#include "ImageSharer.h"
#include "Tile.h"
#include "Bitmap.h"
#include "Binary.h"

App::App(unsigned _argc, const char* _argv[])
{
	std::cout << "Loading configuration file '" << s_config_file_path << "'." << std::endl;

	m_loader = std::make_unique<Loader>(s_config_file_path);
	m_scene = m_loader->make_scene();
	m_image = std::make_unique<Image>(m_loader->image_width(), m_loader->image_height());

	std::cout << "Loaded " << m_scene->materials().size() << " material(s)." << std::endl;
	std::cout << "Loaded " << m_scene->objects().size() << " object(s)." << std::endl;
}

void App::run()
{
	std::cout << "Running Ray Tracer." << std::endl;

	// Compute tiles.
	const unsigned tile_count_x = unsigned(ceil(double(m_image->width()) / s_tile_size));
	const unsigned tile_count_y = unsigned(ceil(double(m_image->height()) / s_tile_size));

	TileQueue tile_queue;

	for (unsigned i = 0; i < tile_count_x; ++i)
	{
		for (unsigned j = 0; j < tile_count_y; ++j)
		{
			unsigned from_x = i * s_tile_size;
			unsigned from_y = j * s_tile_size;
			unsigned to_x = std::min((i + 1) * s_tile_size, m_image->width()) - 1;
			unsigned to_y = std::min((j + 1) * s_tile_size, m_image->height()) - 1;

			tile_queue.queue(new Tile(from_x, from_y, to_x, to_y));
		}
	}
	const size_t original_queue_size = tile_queue.size();

	auto threads = std::make_unique<std::unique_ptr<std::thread>[]>(m_loader->thread_count());
	ImageSharer image_sharer(*m_image);

	Timer render_timer;
	render_timer.start();

	for (unsigned i = 0; i < m_loader->thread_count(); ++i)
	{
		threads.get()[i] = std::make_unique<std::thread>(Ray::Tracer(*m_scene, tile_queue, image_sharer, m_loader->bounce_count()));
	}

	float percentage;
	do
	{
		percentage = (1.f - static_cast<float>(tile_queue.size()) / original_queue_size) * 100.f;
		char percentage_str[32];
		sprintf_s(percentage_str, "Render %06.2f%% completed.\r", percentage);
		std::cout << percentage_str << std::flush;
	}
	while (percentage < 100.);

	std::cout << std::endl;

	// Waiting for all threads to stop.
	for (unsigned i = 0; i < m_loader->thread_count(); ++i)
	{
		threads.get()[i]->join();
	}

	render_timer.stop();
	std::cout << "Rendering completed in " << render_timer.duration_ms() / 1000. << " seconds." << std::endl;

	std::cout << "Writing image to file " << m_loader->output_file() << '.' << std::endl;
	Binary binary;
	img_to_bitmap(m_image.get(), binary);
	write_to_file(m_loader->output_file(), binary);
}
