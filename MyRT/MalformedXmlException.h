#pragma once
#include <stdexcept>

struct MalformedXmlException : public std::runtime_error
{
	explicit MalformedXmlException(const char* _what) noexcept;
};

inline MalformedXmlException::MalformedXmlException(const char* _what) noexcept : std::runtime_error(_what) {}
