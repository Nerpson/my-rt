#pragma once

#include <map>
#include "Pixel.h"

class Image
{
	unsigned m_width;
	unsigned m_height;
	
	std::map<std::pair<unsigned, unsigned>, Pixel*> m_registered_pixels;
	
public:
	Image(unsigned _width, unsigned _height);
	Image(const Image& _image);
	~Image();

	/**
	 * Gets the width of the image.
	 * @return The image's width.
	 */
	unsigned width() const;

	/**
	 * Gets the height of the image.
	 * @return The image's height.
	 */
	unsigned height() const;

	/**
	 * Checks whether a pixel exists at the given position.
	 * @param _x The X position of the pixel.
	 * @param _y The Y position of the pixel.
	 * @return True if the pixel exists, False otherwise.
	 */
	bool pixel_exists_at(unsigned _x, unsigned _y) const;

	/**
	 * Gets the pixel at the given position.
	 * @param _x The X position of the pixel.
	 * @param _y The Y position of the pixel.
	 * @return The pixel at the given position.
	 */
	const Pixel& get_pixel_at(unsigned _x, unsigned _y) const;

	/**
	 * Gets the pixel at the given position.
	 * @param _x The X position of the pixel.
	 * @param _y The Y position of the pixel.
	 * @return The pixel at the given position.
	 */
	Pixel& get_pixel_at(unsigned _x, unsigned _y);

	/**
	 * Voids the pixel at the given position.
	 * @param _x The X position of the pixel to void.
	 * @param _y The Y position of the pixel to void.
	 */
	void remove_pixel_at(unsigned _x, unsigned _y);
};

inline unsigned Image::width() const
{
	return m_width;
}

inline unsigned Image::height() const
{
	return m_height;
}
