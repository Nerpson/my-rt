#pragma once
#include <ostream>
#include "Angle.h"

class Vec3
{
	double m_x, m_y, m_z;

public:
	/**
	 * Gets a unit vector pointing up.
	 * @return A vector pointing up.
	 */
	static Vec3 up();
	
	/**
	 * Gets a unit vector pointing right.
	 * @return A vector pointing right.
	 */
	static Vec3 right();
	
	/**
	 * Gets a unit vector pointing forward.
	 * @return A vector pointing forward.
	 */
	static Vec3 forward();

	/**
	 * Converts a position representation string to a vector object.
	 * @param _pos_string The position representation.
	 * @return The converted position.
	 */
	static Vec3 from_string(const std::string& _pos_string);

	/**
	 * Creates a vector with each of its component randomized between the two given boundaries.
	 * @param _lower_bound The lower random bound (included).
	 * @param _upper_bound The upper random bound (included).
	 * @return The generated vector.
	 */
	static Vec3 random_filled(double _lower_bound, double _upper_bound);

	/**
	 * Calculates the distance between two points.
	 * @param _a The first point.
	 * @param _b The second point.
	 * @return The distance between those points.
	 */
	static double distance(const Vec3& _a, const Vec3& _b);

	/**
	 * Calculates a dot product with two vectors.
	 * @param _a The first vector.
	 * @param _b The second vector.
	 * @return The dot product.
	 */
	static double dot(const Vec3& _a, const Vec3& _b);

	/**
	 * Calculates a cross product with two vectors.
	 * @param _a The first vector.
	 * @param _b The second vector.
	 * @return The cross product.
	 */
	static Vec3 cross(const Vec3& _a, const Vec3& _b);

	/**
	 * Creates a vector.
	 * @param _x The X component.
	 * @param _y The Y component.
	 * @param _z The Z component.
	 */
	Vec3(double _x, double _y, double _z);

	/**
	 * Creates a vector with its components initialized with 0.
	 */
	Vec3();

	/**
	 * Gets the vector's X component.
	 * @return The vector's X component.
	 */
	double x() const;

	/**
	 * Gets the vector's Y component.
	 * @return The vector's Y component.
	 */
	double y() const;

	/**
	 * Gets the vector's Z component.
	 * @return The vector's Z component.
	 */
	double z() const;

	/**
	 * Calculates the vector's norm 2.
	 * @return The vector's norm.
	 */
	double norm() const;

	/**
	 * Checks if the vector is a unit vector.
	 * @return True if the vector is a unit vector, False otherwise.
	 */
	bool is_unit() const;

	/**
	 * Creates a unit vector from the current object.
	 * @return The unit vector.
	 */
	Vec3 to_unit() const;

	/**
	 * Creates a rotated vector from the current object, with the given Euler angles.
	 * @param _yaw The yaw angle.
	 * @param _pitch The pitch angle.
	 * @param _roll The roll angle.
	 * @return The rotated vector.
	 */
	Vec3 rotated(const Angle& _pitch, const Angle& _roll, const Angle& _yaw) const;

	void operator+=(const Vec3& _other);
	void operator-=(const Vec3& _other);

	friend Vec3 operator+(const Vec3& _a, const Vec3& _b);
	friend Vec3 operator-(const Vec3& _a, const Vec3& _b);
	friend Vec3 operator*(const Vec3& _a, double _b);
	friend Vec3 operator*(double _a, const Vec3& _b);
	friend Vec3 operator/(const Vec3& _a, double _b);

	friend bool operator==(const Vec3& _a, const Vec3& _b);

	friend std::ostream& operator<<(std::ostream& _stream, const Vec3& _o);
};

inline Vec3 Vec3::right()
{
	return Vec3(1., 1., 0.);
}

inline Vec3 Vec3::up()
{
	return Vec3(0., 1., 0.);
}

inline Vec3 Vec3::forward()
{
	return Vec3(0., 0., -1.);
}

inline Vec3::Vec3(double _x, double _y, double _z) : m_x(_x), m_y(_y), m_z(_z) {}
inline Vec3::Vec3() : Vec3(0., 0., 0.) {}

inline double Vec3::x() const
{
	return m_x;
}

inline double Vec3::y() const
{
	return m_y;
}

inline double Vec3::z() const
{
	return m_z;
}

inline Vec3 operator*(double _a, const Vec3& _b)
{
	return _b * _a;
}