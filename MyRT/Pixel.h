#pragma once
#include "Color.h"

struct Pixel
{
	double r = 0.;
	double g = 0.;
	double b = 0.;

	/**
	 * Converts this struct's data to a color objet.
	 * @return The pixel's color.
	 */
	Color color() const;

	/**
	 * Sets this struct's data from a color object.
	 * @param _color The color to set.
	 */
	void set_color(const Color& _color);
};

inline Color Pixel::color() const
{
	return Color(r, g, b);
}
