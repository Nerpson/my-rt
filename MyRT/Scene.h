#pragma once
#include <memory>
#include <map>
#include <vector>
#include "Color.h"
#include "Material.h"
#include "Light.h"
#include "Object.h"
#include "Camera.h"

namespace Scene
{
	class Scene
	{
		Color m_backdrop_color;
		std::unique_ptr<std::map<std::string, std::unique_ptr<Material>>> m_materials;
		std::unique_ptr<std::vector<std::unique_ptr<Object>>> m_objects;
		std::unique_ptr<std::vector<std::unique_ptr<Light>>> m_lights;
		std::unique_ptr<Camera> m_camera;
	
	public:
		/**
		 * Gets the scene's backdrop color.
		 * @return The backdrop color.
		 */
		const Color& backdrop_color() const;

		/**
		 * Gets the scene's registered materials.
		 * @return The scene's materials.
		 */
		const std::map<std::string, std::unique_ptr<Material>>& materials() const;

		/**
		 * Gets the scene's objects.
		 * @return The scene's objects.
		 */
		const std::vector<std::unique_ptr<Object>>& objects() const;

		/**
		 * Gets the scene's lights.
		 * @return The scene's lights.
		 */
		const std::vector<std::unique_ptr<Light>>& lights() const;

		/**
		 * Gets the scene's camera.
		 * @return The scene's camera.
		 */
		const Camera& camera() const;

		/**
		 * Creates a scene with prepopulated objects.
		 * @param _materials The scene's registered materials
		 * @param _objects The scene's objects.
		 * @param _lights The scene's lights.
		 * @param _camera The scene's camera.
		 */
		Scene(
			const Color& _backdrop_color,
			std::unique_ptr<std::map<std::string, std::unique_ptr<Material>>>& _materials,
			std::unique_ptr<std::vector<std::unique_ptr<Object>>>& _objects,
			std::unique_ptr<std::vector<std::unique_ptr<Light>>>& _lights,
			std::unique_ptr<Camera>& _camera
		);
	};

	inline Scene::Scene(
		const Color& _backdrop_color,
		std::unique_ptr<std::map<std::string, std::unique_ptr<Material>>>& _materials,
		std::unique_ptr<std::vector<std::unique_ptr<Object>>>& _objects,
		std::unique_ptr<std::vector<std::unique_ptr<Light>>>& _lights,
		std::unique_ptr<Camera>& _camera
	)
		: m_backdrop_color(_backdrop_color),
		m_materials(std::move(_materials)),
		m_objects(std::move(_objects)),
		m_lights(std::move(_lights)),
		m_camera(std::move(_camera))
	{}

	inline const Color& Scene::backdrop_color() const
	{
		return m_backdrop_color;
	}

	inline const std::map<std::string, std::unique_ptr<Material>>& Scene::materials() const
	{
		return *m_materials;
	}

	inline const std::vector<std::unique_ptr<Object>>& Scene::objects() const
	{
		return *m_objects;
	}

	inline const std::vector<std::unique_ptr<Light>>& Scene::lights() const
	{
		return *m_lights;
	}

	inline const Camera& Scene::camera() const
	{
		return *m_camera;
	}
}
