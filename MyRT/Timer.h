#pragma once
#include <chrono>

class Timer
{
	std::chrono::steady_clock::time_point m_start;
	std::chrono::steady_clock::time_point m_end;

public:
	/**
	 * Starts the timer.
	 */
	void start();

	/**
	 * Stops the timer.
	 */
	void stop();

	/**
	 * Gets the timed duration in milliseconds.
	 * @return The duration in milliseconds.
	 */
	long long duration_ms() const;
};

inline void Timer::start()
{
	m_start = std::chrono::steady_clock::now();
}

inline void Timer::stop()
{
	m_end = std::chrono::steady_clock::now();
}

inline long long Timer::duration_ms() const
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(m_end - m_start).count();
}