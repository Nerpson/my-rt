#pragma once
#include <mutex>
#include "Image.h"
#include "Color.h"

class ImageSharer
{
	Image& m_image;
	std::mutex m_mutex;

public:
	/**
	 * Creates an ImageSharer instance to allow multi-thread writing.
	 * @param _image The image to share.
	 */
	ImageSharer(Image& _image);

	/**
	 * Puts a color at the given position.
	 * @param _x The X position of the pixel.
	 * @param _y The Y position of the pixel.
	 * @param _color The color to set.
	 */
	void put_color(unsigned _x, unsigned _y, const Color& _color);

	/**
	 * Gets the image's width.
	 */
	unsigned image_width() const;

	/**
	 * Gets the image's height.
	 */
	unsigned image_height() const;
};

inline ImageSharer::ImageSharer(Image& _image)
	: m_image(_image)
{}

inline unsigned ImageSharer::image_width() const
{
	return m_image.width();
}

inline unsigned ImageSharer::image_height() const
{
	return m_image.height();
}
