#pragma once

class Tile
{
	unsigned m_from_x;
	unsigned m_from_y;
	unsigned m_to_x;
	unsigned m_to_y;
	
public:
	/**
	 * Creates a tile with the specified starting and ending positions.
	 * @param _from_x The starting position's X component.
	 * @param _from_y The starting position's Y component.
	 * @param _to_x The ending position's X component.
	 * @param _to_y The ending position's Y component.
	 */
	Tile(unsigned _from_x, unsigned _from_y, unsigned _to_x, unsigned _to_y);

	/**
	 * Gets the X component of the tile's starting position.
	 * @return The starting position's X component.
	 */
	unsigned from_x() const;

	/**
	 * Gets the Y component of the tile's starting position.
	 * @return The starting position's Y component.
	 */
	unsigned from_y() const;

	/**
	 * Gets the X component of the tile's ending position.
	 * @return The ending position's X component.
	 */
	unsigned to_x() const;

	/**
	 * Gets the Y component of the tile's ending position.
	 * @return The ending position's Y component.
	 */
	unsigned to_y() const;
};

inline unsigned Tile::from_x() const
{
	return m_from_x;
}

inline unsigned Tile::from_y() const
{
	return m_from_y;
}

inline unsigned Tile::to_x() const
{
	return m_to_x;
}

inline unsigned Tile::to_y() const
{
	return m_to_y;
}
