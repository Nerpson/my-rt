#pragma once
#include <string>
#include "Color.h"

namespace Scene
{
	class Material
	{
		const std::string m_id;
		const Color m_diffuse;
		const double m_specular;

	public:
		Material(const std::string& _id, const Color& _diffuse, double _specular);

		const std::string& id() const;
		const Color& diffuse() const;
		double specular() const;
	};

	inline Material::Material(const std::string& _id, const Color& _diffuse, double _specular) :
		m_id(_id), m_diffuse(_diffuse), m_specular(_specular)
	{}

	inline const std::string& Material::id() const
	{
		return m_id;
	}

	inline const Color& Material::diffuse() const
	{
		return m_diffuse;
	}

	inline double Material::specular() const
	{
		return m_specular;
	}
}
