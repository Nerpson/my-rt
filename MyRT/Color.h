#pragma once
#include <ostream>

class Color
{
	double m_r, m_g, m_b;

public:
	/**
	 * Creates a color from RGB components.
	 * @param _r The red component.
	 * @param _g The green component.
	 * @param _b The blue component.
	 */
	Color(double _r, double _g, double _b);

	/**
	 * Copies a color.
	 * @param _other The color to copy.
	 */
	Color(const Color& _other);

	/** 
	 * Creates a black color.
	 */
	Color();

	/**
	 * Gets the color's red component.
	 * @return The color's red component.
	 */
	double r() const;
	
	/**
	 * Gets the color's green component.
	 * @return The color's green component.
	 */
	double g() const;
	
	/**
	 * Gets the color's blue component.
	 * @return The color's blue component.
	 */
	double b() const;

	/**
	 * Creates a new color with its component ranged between two values.
	 * @param _lower_bound The lower bound of the range.
	 * @param _upper_bound The upper bound of the range.
	 * @return The ranged color.
	 */
	Color to_ranged(double _lower_bound, double _upper_bound) const;

	/**
	 * Creates a Color instance from an hexadecimal representation ("#xxx" or "#xxxxxx").
	 * @param _color_string The hexadecimal representation string.
	 * @return The corresponding color.
	 */
	static Color from_web(const char* _color_string);

	void operator=(const Color& _other);

	void operator+=(const Color& _other);
	void operator-=(const Color& _other);

	friend Color operator+(const Color& _a, const Color& _b);
	friend Color operator*(const Color& _a, const Color& _b);
	friend Color operator*(const Color& _a, double _b);
	friend Color operator/(const Color& _a, double _b);
	
	friend bool operator==(const Color& _a, const Color& _b);
	friend std::ostream& operator<<(std::ostream& _stream, const Color& _o);
};

inline Color::Color(double _r, double _g, double _b) : m_r(_r), m_g(_g), m_b(_b) {}
inline Color::Color(const Color& _other) : Color(_other.m_r, _other.m_g, _other.m_b) {}
inline Color::Color() : Color(0., 0., 0.) {}

inline double Color::r() const
{
	return m_r;
}

inline double Color::g() const
{
	return m_g;
}

inline double Color::b() const
{
	return m_b;
}