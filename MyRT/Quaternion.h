#pragma once
#include "Vec3.h"
#include "Angle.h"

struct Quaternion
{
	double w;
	double x;
	double y;
	double z;

	/**
	 * Creates a quaternion from the given Euler angles.
	 * @param _yaw The yaw angle.
	 * @param _pitch The pitch angle.
	 * @param _roll The roll angle.
	 * @return The created Quaternion.
	 */
	static Quaternion from_angles(const Angle& _yaw, const Angle& _pitch, const Angle& _roll);

	/**
	 * Rotates a vector.
	 * @param _vec3 The vector to rotate.
	 * @return The rotated vector.
	 */
	Vec3 rotate(const Vec3& _vec3) const;
};

