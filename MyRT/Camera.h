#pragma once
#include "Vec3.h"
#include "Angle.h"

namespace Scene
{
	class Camera
	{
		Vec3 m_position;
		Angle m_fov = Angle::from_degrees(90.f);
		Angle m_yaw;
		Angle m_pitch;
		Angle m_roll;

	public:
		/**
		 * Gets the camera's position.
		 * @return The camera's position.
		 */
		const Vec3& position() const;

		/**
		 * Gets the camera's yaw angle.
		 * @return The camera's yaw.
		 */
		const Angle& yaw() const;

		/**
		 * Gets the camera's pitch angle.
		 * @return The camera's pitch.
		 */
		const Angle& pitch() const;

		/**
		 * Gets the camera's roll angle.
		 * @return The camera's roll.
		 */
		const Angle& roll() const;

		/**
		 * Gets the camera's field of view.
		 * @return The camera's field of view.
		 */
		const Angle& fov() const;

		/**
		 * Sets the camera's position.
		 * @param _position The position to set.
		 */
		void set_position(const Vec3& _position);

		/**
		 * Sets the camera's position.
		 * @param _position The position to set.
		 */
		void set_yaw(const Angle& _yaw);

		/**
		 * Sets the camera's pitch.
		 * @param _pitch The pitch to set.
		 */
		void set_pitch(const Angle& _pitch);

		/**
		 * Sets the camera's roll.
		 * @param _roll The roll to set.
		 */
		void set_roll(const Angle& _roll);

		/**
		 * Sets the camera's field of view.
		 * @param _fov The field of view to set.
		 */
		void set_fov(const Angle& _fov);
	};

	inline const Vec3& Camera::position() const
	{
		return m_position;
	}

	inline const Angle& Camera::yaw() const
	{
		return m_yaw;
	}

	inline const Angle& Camera::pitch() const
	{
		return m_pitch;
	}

	inline const Angle& Camera::roll() const
	{
		return m_roll;
	}

	inline const Angle& Camera::fov() const
	{
		return m_fov;
	}

	inline void Camera::set_position(const Vec3& _position)
	{
		m_position = _position;
	}

	inline void Camera::set_yaw(const Angle& _yaw)
	{
		m_yaw = _yaw;
	}

	inline void Camera::set_pitch(const Angle& _pitch)
	{
		m_pitch = _pitch;
	}

	inline void Camera::set_roll(const Angle& _roll)
	{
		m_roll = _roll;
	}

	inline void Camera::set_fov(const Angle& _fov)
	{
		m_fov = _fov;
	}

}
