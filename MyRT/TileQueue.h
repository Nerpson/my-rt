#pragma once
#include <queue>
#include <mutex>
#include "Tile.h"

class TileQueue
{
	std::queue<Tile*> m_queue;
	std::mutex m_mutex;

public:
	/**
	 * Gets the number of tiles remaining in the queue.
	 * @return The number of tiles in the queue.
	 */
	size_t size() const;

	/**
	 * Adds a tile at the end of the queue.
	 * @param _tile The tile to append.
	 */
	void queue(Tile* _tile);

	/**
	 * Returns the next tile and removes it from the queue.
	 * @return The next tile to be processed.
	 */
	Tile* next();
};

inline size_t TileQueue::size() const
{
	return m_queue.size();
}
