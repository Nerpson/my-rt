#pragma once
#include <stdexcept>

namespace Scene
{
	enum class EObjectType
	{
		Plane, Sphere
	};

	const char* object_type_to_string(const EObjectType& _object_type);
	bool object_type_exists(const char* _object_type);
}
