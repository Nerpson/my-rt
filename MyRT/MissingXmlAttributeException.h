#pragma once
#include "MalformedXmlException.h"

class MissingXmlAttributeException final : public MalformedXmlException
{
	std::string m_xml_node_name;
	std::string m_missing_xml_attribute_name;
	std::string m_what;

public:
	explicit MissingXmlAttributeException(const std::string& _xml_node_name, const std::string&  _missing_xml_attribute_name) noexcept;
	const char* what() const noexcept override;

	const char* xml_node_name() const noexcept;
	const char* missing_xml_attribute_name() const noexcept;
};

inline const char* MissingXmlAttributeException::what() const noexcept
{
	return m_what.c_str();
}

inline const char* MissingXmlAttributeException::xml_node_name() const noexcept
{
	return m_xml_node_name.c_str();
}

inline const char* MissingXmlAttributeException::missing_xml_attribute_name() const noexcept
{
	return m_missing_xml_attribute_name.c_str();
}
