#include "Random.h"
#include <random>
#include <chrono>
#include <ctime>

unsigned Random::time_since_midnight()
{
	using namespace std::chrono;
	auto chrono_now = system_clock::now();
	auto time_now = system_clock::to_time_t(chrono_now);
	tm date;
	localtime_s(&date, &time_now);
	date.tm_hour = 0;
	date.tm_min = 0;
	date.tm_sec = 0;
	return static_cast<unsigned>((chrono_now - system_clock::from_time_t(std::mktime(&date))).count());
}

double Random::get(double _lower_bound, double _upper_bound)
{
	std::mt19937 random_generator(time_since_midnight());
	std::uniform_real_distribution<double> distribution(_lower_bound, _upper_bound);
	return distribution(random_generator);
}
