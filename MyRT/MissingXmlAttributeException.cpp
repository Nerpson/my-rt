#include "MissingXmlAttributeException.h"
#include <sstream>

MissingXmlAttributeException::MissingXmlAttributeException(const std::string& _xml_node_name, const std::string& _missing_xml_attribute_name) noexcept
	: MalformedXmlException(""), m_xml_node_name(_xml_node_name), m_missing_xml_attribute_name(_missing_xml_attribute_name)
{
	std::stringstream ss;
	ss << "Missing \"" << m_missing_xml_attribute_name << "\" attribute on \"" << m_xml_node_name << "\" node.";
	m_what = ss.str();
}
