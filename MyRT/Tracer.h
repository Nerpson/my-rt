#pragma once
#include "Scene.h"
#include "TileQueue.h"
#include "ImageSharer.h"

namespace Ray
{
	class Tracer
	{
		const Scene::Scene& m_scene;
		TileQueue& m_tile_queue;
		ImageSharer& m_image_sharer;
		unsigned m_max_depth;

		Color cast_ray(const Ray& _ray, unsigned _depth = 0u);
		void compute_tile(const Tile& _tile);

	public:
		/**
		 * Creates a Tracer instance for a thread.
		 * @param _scene The scene to render.
		 * @param _tile_queue The queue to get the tiles to render from.
		 * @param _image_sharer The ImageSharer object to write the pixels to.
		 */
		Tracer(const Scene::Scene& _scene, TileQueue& _tile_queue, ImageSharer& _image_sharer, unsigned _max_depth);

		void operator()();
	};

	inline Tracer::Tracer(const Scene::Scene& _scene, TileQueue& _tile_queue, ImageSharer& _image_sharer, unsigned _max_depth) :
		m_scene(_scene), m_tile_queue(_tile_queue), m_image_sharer(_image_sharer), m_max_depth(_max_depth)
	{}
}
