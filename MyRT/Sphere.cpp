#include "Sphere.h"

namespace Scene
{
	bool Sphere::intersects(Ray::Ray _ray, Vec3& _intersection, Vec3& _normal) const
	{
		const double b = 2. * Vec3::dot(_ray.origin - position(), _ray.direction);
		const double c = Vec3::dot(_ray.origin - position(), _ray.origin - position()) - pow(m_radius, 2.);
		const double delta = pow(b, 2.) - 4. * c;

		bool intersecting(false);

		if (delta == 0.)
		{
			const double t = -b;
			if (MIN_INTERSECTION_DISTANCE <= t)
			{
				_intersection = _ray.origin + _ray.direction * (t / 2.);
				intersecting = true;
			}
		}
		else if (0 < delta)
		{
			const double sqrt_delta = sqrt(delta);
			const double t1 = -b + sqrt_delta;
			const double t2 = -b - sqrt_delta;
			if (MIN_INTERSECTION_DISTANCE <= t2)
			{
				_intersection = _ray.origin + _ray.direction * (t2 / 2.);
				intersecting = true;
			}
			else if (MIN_INTERSECTION_DISTANCE <= t1)
			{
				_intersection = _ray.origin + _ray.direction * (t1 / 2.);
				intersecting = true;
			}
		}

		if (intersecting) _normal = (_intersection - position()).to_unit();
		return intersecting;
	}
}