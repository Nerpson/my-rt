#include <cstdlib>
#include <iostream>
#include "App.h"

int main(int _argc, const char* _argv[])
{
	try
	{
		App app(_argc, _argv);
		app.run();
	}
	catch (const std::runtime_error& e)
	{
		std::cerr << "An error occurred: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
